
# Wild project

## Code of conduct and licenses.

[![Contributor 
Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)

The [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) applies.

All code is dual licensed as MIT + Apache-2.0 unless specified otherwise.

All documentation is licensed CC0 unless specified otherwise.

All references are free of patents as far as contributors are aware unless
stated otherwise.

All contributors certify their contributions under the [Developer Certificate
of Origin](https://developercertificate.org/) by signing their commits.

## Contributors

Before your contributions may be accepted, you must first sign the Developer
Certificate of Origin, or DCO for short. Please refer to the
[/meta/](https://gitea.com/wild/meta) repository for further information.
